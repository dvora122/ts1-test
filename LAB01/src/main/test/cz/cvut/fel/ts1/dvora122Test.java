package cz.cvut.fel.ts1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class dvora122Test {

    @Test
    void factorial() {
        assertEquals(6, dvora122.factorial(3));
        assertEquals(120, dvora122.factorial(5));
    }
}