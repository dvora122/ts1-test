package cz.cvut.fel.ts1;

public class dvora122 {
    public static long factorial (int n){

        if (n == 0) {
            return 1;
        } else {
            return (n * factorial(n - 1));
        }
    }
}
